using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class CustomButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [Serializable]
    public class ButtonClickedEvent : UnityEvent { }

    [FormerlySerializedAs("OnHold")]
    [SerializeField]
    private ButtonClickedEvent onHold = new ButtonClickedEvent();

    bool pressed = false;


    private void Update()
    {
        if(pressed)
            onHold?.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        pressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pressed = false;
    }
}
