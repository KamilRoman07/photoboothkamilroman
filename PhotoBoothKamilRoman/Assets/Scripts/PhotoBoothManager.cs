using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PhotoBoothManager : MonoBehaviour
{
    [SerializeField] Transform spawnObjectContainer;
    [SerializeField] Transform lookAtPoint;
    [SerializeField] float rotationSpeed = 1;
    [SerializeField] float movementSpeed = 1;
    [SerializeField] float enableUIAfterPhotoDelay = 1f;
    [SerializeField] GameObject UI;
    List<GameObject> objectsToSpawn;
    GameObject spawnedObject;
    int currentIndex = 0;

    private void Awake()
    {
        objectsToSpawn = Resources.LoadAll<GameObject>("Input").AsEnumerable().ToList();
        if (objectsToSpawn.Count > 0)
            SpawnObject(currentIndex);
        else
            Debug.LogError("There are no objects in requested folder");
    }

    #region Spawn Controlls
    private void SpawnObject(int index)
    {
        if (spawnObjectContainer.childCount > 0)
        {
            Destroy(spawnObjectContainer.GetChild(0).gameObject);
        }
        spawnedObject = Instantiate(objectsToSpawn[index]);
        spawnedObject.transform.SetParent(spawnObjectContainer, false);
        spawnedObject.transform.localPosition = Vector3.zero;
    }

    public void SpawnNextObject()
    {
        if (objectsToSpawn.Count > 0) 
        { 
            if (currentIndex + 1 == objectsToSpawn.Count)
                currentIndex = 0;
            else
                currentIndex++;
            SpawnObject(currentIndex);
        }
        else
            Debug.LogError("There are no objects in requested folder");
    }

    public void SpawnPreviousObject()
    {
        if (objectsToSpawn.Count > 0)
        {
            if (currentIndex - 1 == 0)
                currentIndex = objectsToSpawn.Count - 1;
            else
                currentIndex--;
            SpawnObject(currentIndex);
        }
        else
            Debug.LogError("There are no objects in requested folder");
    }
    #endregion

    #region Rotate Controlls
    public void RotateObjectLeftRight(bool left)
    {
        spawnObjectContainer.Rotate(0, (left ? -1 : 1) * rotationSpeed, 0);
    }

    public void RotateObjectHorizontally(bool front)
    {
        if(spawnedObject)
            spawnedObject.transform.Rotate((front ? -1 : 1) * rotationSpeed, 0, 0);
    }

    public void RotateObjectVertically(bool up)
    {
        if(spawnedObject)
            spawnedObject.transform.Rotate(0, 0, (up ? -1 : 1) * rotationSpeed);
    }
    #endregion

    #region Move Controlls
    public void MoveObjectUpDown(int upDown)
    {
        spawnObjectContainer.transform.position = spawnObjectContainer.transform.position + upDown * Camera.main.transform.up * movementSpeed;
    }

    public void MoveObjectLeftRight(int leftRight)
    {
        spawnObjectContainer.transform.position = spawnObjectContainer.transform.position + leftRight * Camera.main.transform.right * movementSpeed;
    }

    public void MoveObjectForwardBackward(int forwardBackward)
    {
        spawnObjectContainer.transform.position = spawnObjectContainer.transform.position + forwardBackward * Camera.main.transform.forward * movementSpeed;
    }
    #endregion

    public void MakePhoto()
    {
        StartCoroutine(MakePhotoCoroutine());
    }

    public IEnumerator MakePhotoCoroutine()
    {
        UI.SetActive(false);
        ScreenCapture.CaptureScreenshot(Application.dataPath + "/Output/" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png");
        Debug.LogError("ScreenshotTaken");
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
        yield return new WaitForSeconds(enableUIAfterPhotoDelay);
        UI.SetActive(true);
    }
}
