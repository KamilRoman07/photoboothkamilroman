using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControlls : MonoBehaviour
{
    [SerializeField] Transform lookAtPoint;
    [SerializeField] Transform spawnObjectContainer;
    [SerializeField] CinemachineFreeLook mainCinemachineCamera;
    [SerializeField] KeyCode buttonToAllowCameraMovement = KeyCode.Mouse1;

    [Header("--Zoom--")]
    [SerializeField] float zoomStepValue = 1;
    [SerializeField] float zoomMinimumValue = 0.5f;
    [SerializeField] float zoomMaxValue = 10;
    [Header("--Rotation--")]
    [SerializeField] float rotationSpeed = 1;

    private void Update()
    {
        if (Input.GetKey(buttonToAllowCameraMovement))
        {
            RotateCamera(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        }
        else
        {
            RotateCamera(0, 0);
        }

    }

    public void RotateCamera(float xAxisFactor, float yAxisFactor)
    {
        if (mainCinemachineCamera)
        {
            mainCinemachineCamera.m_XAxis.Value = -xAxisFactor * rotationSpeed;
            mainCinemachineCamera.m_YAxis.Value = Mathf.Clamp(mainCinemachineCamera.m_YAxis.Value + yAxisFactor * 0.01f, 0f, 1f);
        }
    }

    public void ZoomInOut(bool inOut)
    {
        if (mainCinemachineCamera)
        {
            mainCinemachineCamera.m_Orbits[0].m_Radius = Mathf.Clamp(mainCinemachineCamera.m_Orbits[0].m_Radius + (inOut ? -zoomStepValue : zoomStepValue) * 0.01f, zoomMinimumValue, zoomMaxValue);
            mainCinemachineCamera.m_Orbits[1].m_Radius = Mathf.Clamp(mainCinemachineCamera.m_Orbits[1].m_Radius + (inOut ? -zoomStepValue : zoomStepValue) * 0.01f, zoomMinimumValue, zoomMaxValue);
            mainCinemachineCamera.m_Orbits[2].m_Radius = Mathf.Clamp(mainCinemachineCamera.m_Orbits[2].m_Radius + (inOut ? -zoomStepValue : zoomStepValue) * 0.01f, zoomMinimumValue, zoomMaxValue);
        }
    }

    public void ResetCameraPosition()
    {
        lookAtPoint.position = spawnObjectContainer.position;
    }
}
